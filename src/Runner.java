import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.ConnectionFactory;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Класс запуска.
 */
public class Runner extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
       Parent root = FXMLLoader.load(getClass().getResource("view/FormView.fxml"));
        primaryStage.setTitle("Справочник телефонов");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public static void main(String[] args) {

        try {
            Connection connection = new ConnectionFactory().getConnection();
            System.out.println("Соединение с базой успешно.");
            connection.close();
        } catch (RuntimeException e) {
            System.out.println("Соединиться с базой не удалось.");
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        launch(args);
    }
}
