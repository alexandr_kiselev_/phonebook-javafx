package beans;

/**
 * Класс содержит информацию о структурном подразделении.
 */
public class Subdivision {
    /**
     * id структурного подразделения.
     */
    private int id;
    /**
     * Название структурного подразделения.
     */
    private String subdivisionName;
    /**
     * id родителя структурного подразделения.
     */
    private Integer parent_id;

    public Subdivision() {
    }

    public Subdivision(int id, String subdivisionName) {
        this.id = id;
        this.subdivisionName = subdivisionName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubdivisionName() {
        return subdivisionName;
    }

    public void setSubdivisionName(String subdivisionName) {
        this.subdivisionName = subdivisionName;
    }

    public Integer getParent_id() {
        return parent_id;
    }

    public void setParent_id(Integer parent_id) {
        this.parent_id = parent_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Subdivision that = (Subdivision) o;

        if (id != that.id) return false;
        if (subdivisionName != null ? !subdivisionName.equals(that.subdivisionName) : that.subdivisionName != null)
            return false;
        return parent_id != null ? parent_id.equals(that.parent_id) : that.parent_id == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (subdivisionName != null ? subdivisionName.hashCode() : 0);
        result = 31 * result + (parent_id != null ? parent_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {

        return /*id + ". " +*/ subdivisionName;
    }
}
