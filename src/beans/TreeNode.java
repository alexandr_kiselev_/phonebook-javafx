package beans;

import java.util.LinkedList;
import java.util.List;

/**
 * Класс древовидной структуры класса Т.
 *
 * @param <T>.
 */
public class TreeNode<T> {

    /**
     * Экземпляр класса.
     */
    T data;
    /**
     * Сылка на родителя.
     */
    TreeNode<T> parent;
    /**
     * Сылка на потомков.
     */
    List<TreeNode<T>> children;

    /**
     * Корень дерева.
     *
     * @param data .
     */
    public TreeNode(T data) {
        this.data = data;
        this.children = new LinkedList<TreeNode<T>>();
    }

    /**
     * Добавление потомка к текущему экземпляру.
     *
     * @param child .
     * @return
     */
    public TreeNode<T> addChild(T child) {
        TreeNode<T> childNode = new TreeNode<T>(child);
        childNode.parent = this;
        this.children.add(childNode);
        return childNode;

    }

    public TreeNode<T> getParent() {
        return parent;
    }

    public void setParent(TreeNode<T> parent) {
        this.parent = parent;
    }

    public List<TreeNode<T>> getChildren() {
        return children;
    }

    public T getData() {
        return data;
    }
}
