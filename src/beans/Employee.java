package beans;

/**
 * Класс содержит информацию о сотрудниках.
 */
public class Employee {
    /**
     * id сотрудника.
     */
    private int id;
    /**
     * ФИО.
     */
    private String fullName;
    /**
     * Должность.
     */
    private String position;
    /**
     * Служебные номера телеофнов.
     */
    private String businessPhoneNumber;
    /**
     * Личные номера телеофнов.
     */
    private String personalPhoneNumber;
    /**
     * Служебные мобильные номера телеофнов.
     */
    private String businessMobilePhoneNumber;

    /**
     * id структурного подразделения.
     */
    private int subdivisionId;
    /**
     * Название структурного подразделения.
     */
    private String subdivisionName;

    public Employee() {
    }

    public Employee(final int id, final String fullName, final String position,
                    final String businessPhoneNumber, final String personalPhoneNumber,
                    final String businessMobilePhoneNumber) {
        this.id = id;
        this.fullName = fullName;
        this.position = position;
        this.businessPhoneNumber = businessPhoneNumber;
        this.personalPhoneNumber = personalPhoneNumber;
        this.businessMobilePhoneNumber = businessMobilePhoneNumber;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getBusinessPhoneNumber() {
        return businessPhoneNumber;
    }

    public void setBusinessPhoneNumber(String businessPhoneNumber) {
        this.businessPhoneNumber = businessPhoneNumber;
    }

    public String getPersonalPhoneNumber() {
        return personalPhoneNumber;
    }

    public void setPersonalPhoneNumber(String personalPhoneNumber) {
        this.personalPhoneNumber = personalPhoneNumber;
    }

    public String getBusinessMobilePhoneNumber() {
        return businessMobilePhoneNumber;
    }

    public void setBusinessMobilePhoneNumber(String businessMobilePhoneNumber) {
        this.businessMobilePhoneNumber = businessMobilePhoneNumber;
    }

    public int getSubdivisionId() {
        return subdivisionId;
    }

    public void setSubdivisionId(int subdivisionId) {
        this.subdivisionId = subdivisionId;
    }

    public String getSubdivisionName() {
        return subdivisionName;
    }

    public void setSubdivisionName(String subdivisionName) {
        this.subdivisionName = subdivisionName;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", position='" + position + '\'' +
                ", businessPhoneNumber='" + businessPhoneNumber + '\'' +
                ", personalPhoneNumber='" + personalPhoneNumber + '\'' +
                ", businessMobilePhoneNumber='" + businessMobilePhoneNumber + '\'' +
                ", subdivisionId='" + subdivisionId + '\'' +
                ", subdivisionName='" + subdivisionName + '\'' +
                '}';

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (id != employee.id) return false;
        if (subdivisionId != employee.subdivisionId) return false;
        if (fullName != null ? !fullName.equals(employee.fullName) : employee.fullName != null) return false;
        if (position != null ? !position.equals(employee.position) : employee.position != null) return false;
        if (businessPhoneNumber != null ? !businessPhoneNumber.equals(employee.businessPhoneNumber) : employee.businessPhoneNumber != null)
            return false;
        if (personalPhoneNumber != null ? !personalPhoneNumber.equals(employee.personalPhoneNumber) : employee.personalPhoneNumber != null)
            return false;
        if (businessMobilePhoneNumber != null ? !businessMobilePhoneNumber.equals(employee.businessMobilePhoneNumber) : employee.businessMobilePhoneNumber != null)
            return false;
        if (subdivisionName != null ? !subdivisionName.equals(employee.subdivisionName) : employee.subdivisionName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 13 * result + (fullName != null ? fullName.hashCode() : 0);
        result = 13 * result + (position != null ? position.hashCode() : 0);
        result = 13 * result + (businessPhoneNumber != null ? businessPhoneNumber.hashCode() : 0);
        result = 13 * result + (personalPhoneNumber != null ? personalPhoneNumber.hashCode() : 0);
        result = 13 * result + (businessMobilePhoneNumber != null ? businessMobilePhoneNumber.hashCode() : 0);
        result = 13 * result + subdivisionId;
        result = 13 * result + (subdivisionName != null ? subdivisionName.hashCode() : 0);
        return result;
    }
}

