package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Создание соединения с БД.
 */
public class ConnectionFactory {
    public Connection getConnection() {
        try {
            //путь к БД postgresql, логин, пароль
            return DriverManager.getConnection("jdbc:postgresql://localhost/postgres?user=postgres&password=root");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
