package controller;

import beans.Employee;
import beans.Subdivision;
import beans.TreeNode;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import model.EmployeeDAO;
import model.SubdivisionDAO;

/**
 * Класс контроллера формы.
 */
public class ControllerFormView implements Initializable {
    /**
     * Экземпляр обекта доступа к БД класса сотрудкик.
     */
    private EmployeeDAO employeeDAO;
    /**
     * Экземпляр обекта доступа к БД класса подразделение.
     */
    private SubdivisionDAO subdivisionDAO;

    /**
     * Получение id всех потомков структурного подразделения.
     */
    private void getAllChildrenIdFromSubdivision(TreeItem<Subdivision> stringTreeItem, List<Integer> integerList) {
        for (TreeItem<Subdivision> treeItem : stringTreeItem.getChildren()
                ) {
            integerList.add(treeItem.getValue().getId());
            getAllChildrenIdFromSubdivision(treeItem, integerList);
        }
    }

    /**
     * Построение дервева структурного подраздееления для представления в TreeView.
     */
    private void buildSubdivisionTreeFromRoot(TreeItem<Subdivision> stringTreeItem, TreeNode<Subdivision> subdivisionTreeNode) {
        if (subdivisionTreeNode.getChildren() != null)
            for (TreeNode<Subdivision> treeNode : subdivisionTreeNode.getChildren()
                    ) {
                TreeItem<Subdivision> treeItem = new TreeItem<>(treeNode.getData());
                treeItem.setExpanded(true);//открытие ветки
                stringTreeItem.getChildren().add(treeItem);
                buildSubdivisionTreeFromRoot(treeItem, treeNode);
            }
    }

    /**
     * Проверка введенных данных для добавления/редактирования сотрудника.
     */
    private boolean checkEmployeeFieldForAddEmployee() {
        if (fullNameTextEdit.getText() == null || fullNameTextEdit.getText().isEmpty()) {
            showErrorDialog("Введите ФИО");
            fullNameTextEdit.requestFocus();
            return false;
        } else if (positionTextEdit.getText() == null || positionTextEdit.getText().isEmpty()) {
            showErrorDialog("Введите должность");
            positionTextEdit.requestFocus();
            return false;
        } else if (businessPhoneNumberTextArea.getText() == null || businessPhoneNumberTextArea.getText().isEmpty()) {
            showErrorDialog("Введите номер телефона");
            businessPhoneNumberTextArea.requestFocus();
            return false;
        } else if (personalPhoneNumberTextArea.getText() == null || personalPhoneNumberTextArea.getText().isEmpty()) {
            showErrorDialog("Введите номер телефона");
            personalPhoneNumberTextArea.requestFocus();
            return false;
        } else if (businessMobilePhoneNumberTextArea.getText() == null || businessMobilePhoneNumberTextArea.getText().isEmpty()) {
            showErrorDialog("Введите номер телефона");
            businessMobilePhoneNumberTextArea.requestFocus();
            return false;
        } else if (subdivisionTreeView.getSelectionModel().getSelectedItem() == null) {
            showErrorDialog("Выберите подразделение");
            subdivisionTreeView.requestFocus();
            return false;
        }
        return true;
    }

    /**
     * Поиск по введенным данным и выбранному подразделению.
     */
    private void searchByEmployeeAndSubdivision(Employee employee) {
        ArrayList<Integer> subdivisionIdList = new ArrayList<>();

        if (subdivisionTreeView.getSelectionModel().getSelectedItem() != null) {
            subdivisionIdList.add(subdivisionTreeView.getSelectionModel().getSelectedItem().getValue().getId());
            getAllChildrenIdFromSubdivision(subdivisionTreeView.getSelectionModel().getSelectedItem(), subdivisionIdList);
        }

        try {
            List<Employee> employeeList = employeeDAO.searchEmployee(employee, subdivisionIdList);

            if (employeeList.isEmpty()) {
                showInformationDialog("По данным критериям поиска ничего не найдено.");
            } else {
                tableViewEmployees.setItems(FXCollections.observableArrayList(employeeList));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private TableColumn<Employee, Integer> idEmployees;

    @FXML
    private TableColumn<Employee, String> positionEmployees;

    @FXML
    private TableColumn<Employee, String> fullNameEmployees;

    @FXML
    private TableColumn<Employee, String> subdivisionEmployees;

    @FXML
    private TableColumn<Employee, String> businessPhoneNumberEmployees;

    @FXML
    private TableColumn<Employee, String> personalPhoneNumberEmployees;

    @FXML
    private TableColumn<Employee, String> businessMobilePhoneNumberEmployees;

    @FXML
    private TableView<Employee> tableViewEmployees;

    @FXML
    private TreeTableColumn<?, ?> subdivision;

    @FXML
    private TreeTableColumn<?, ?> businessMobileNumber;

    @FXML
    private TreeTableColumn<?, ?> fullName1;

    @FXML
    private TreeTableColumn<?, ?> businessNumber;

    @FXML
    private TreeTableColumn<?, ?> position1;

    @FXML
    private TreeTableColumn<?, ?> personalNumber;

    @FXML
    private TreeTableView<?> tableSubdivisions;

    @FXML
    private TreeView<Subdivision> subdivisionTreeView;

    @FXML
    private Button removeEmployeeButton;

    @FXML
    private Button searchEmployeeButton;

    @FXML
    private Button addEmployeeButton;

    @FXML
    private Button editEmployeeButton;

    @FXML
    private Button addSubdivisionButton;

    @FXML
    private Button filterBySubdivisionButton;

    @FXML
    private Button removeSubdivisionButton;

    @FXML
    private Button resetFilterBySubdivisionButton;

    @FXML
    private Button editSubdivisionButton;

    @FXML
    private TextField positionTextEdit;

    @FXML
    private TextField subdivisionTextEdit;

    @FXML
    private TextField fullNameTextEdit;

    @FXML
    private TextArea businessPhoneNumberTextArea;

    @FXML
    private TextArea personalPhoneNumberTextArea;

    @FXML
    private TextArea businessMobilePhoneNumberTextArea;

    @FXML
    void clearInputFields(ActionEvent event) {
        fullNameTextEdit.setText("");
        positionTextEdit.setText("");
        businessPhoneNumberTextArea.setText("");
        personalPhoneNumberTextArea.setText("");
        businessMobilePhoneNumberTextArea.setText("");
        subdivisionTextEdit.setText("");
    }

    @FXML
    /**
     * Действия при нажатии кнопки добавить сотрудника.
     */
    void addEmployeeButtonAction(ActionEvent event) {
        if (checkEmployeeFieldForAddEmployee()) {
            Employee employee = new Employee(-1, fullNameTextEdit.getText(), positionTextEdit.getText(),
                    businessPhoneNumberTextArea.getText(), personalPhoneNumberTextArea.getText(),
                    businessMobilePhoneNumberTextArea.getText());
            employee.setSubdivisionId(subdivisionTreeView.getSelectionModel().getSelectedItem().getValue().getId());
            employee.setSubdivisionName(subdivisionTreeView.getSelectionModel().getSelectedItem().getValue().getSubdivisionName());
            employeeDAO.addNewEmployee(employee);

            if (employee.getId() > 0)
                tableViewEmployees.getItems().add(employee);
            else
                showErrorDialog("Не получилось добавить сотрудника.");
        }
    }

    @FXML
    /**
     * Действия при нажатии кнопки удалить сотрудника(ов).
     */
    void removeEmployeeButtonAction(ActionEvent event) {

        if (tableViewEmployees.getSelectionModel().getSelectedItems() != null)
            if (showConfirmationDialog("Вы действительно хоти удалить выделенные записи?")) {
                for (Employee employee : tableViewEmployees.getSelectionModel().getSelectedItems()
                        ) {
                    employeeDAO.removeEmployee(employee.getId());
                    tableViewEmployees.getItems().remove(employee);
                }
            }
    }

    @FXML
    /**
     * Действия при нажатии кнопки редактровать сотрудника.
     */
    void editEmployeeButtonAction(ActionEvent event) {
        if (checkEmployeeFieldForAddEmployee()) {
            Employee employee = new Employee(tableViewEmployees.getSelectionModel().getSelectedItem().getId(),
                    fullNameTextEdit.getText(), positionTextEdit.getText(),
                    businessPhoneNumberTextArea.getText(), personalPhoneNumberTextArea.getText(),
                    businessMobilePhoneNumberTextArea.getText());
            employee.setSubdivisionId(subdivisionTreeView.getSelectionModel().getSelectedItem().getValue().getId());
            employee.setSubdivisionName(subdivisionTreeView.getSelectionModel().getSelectedItem().getValue().getSubdivisionName());
            employeeDAO.updateEmployee(employee);

            //обновление строки TableView
            tableViewEmployees.getSelectionModel().getSelectedItem().setFullName(employee.getFullName());
            tableViewEmployees.getSelectionModel().getSelectedItem().setPosition(employee.getPosition());
            tableViewEmployees.getSelectionModel().getSelectedItem().setBusinessPhoneNumber(employee.getBusinessPhoneNumber());
            tableViewEmployees.getSelectionModel().getSelectedItem().setPersonalPhoneNumber(employee.getPersonalPhoneNumber());
            tableViewEmployees.getSelectionModel().getSelectedItem().setBusinessMobilePhoneNumber(employee.getBusinessMobilePhoneNumber());
            tableViewEmployees.getSelectionModel().getSelectedItem().setSubdivisionId(employee.getSubdivisionId());
            tableViewEmployees.getSelectionModel().getSelectedItem().setSubdivisionName(employee.getSubdivisionName());
            tableViewEmployees.refresh();
        }
    }

    @FXML
    /**
     * Действия при нажатии кнопки поиск сотрудника.
     */
    void searchEmployeeButtonAction(ActionEvent event) {
        Employee employee = new Employee();

        employee.setFullName(fullNameTextEdit.getText());
        employee.setPosition(positionTextEdit.getText());
        employee.setBusinessPhoneNumber(businessPhoneNumberTextArea.getText());
        employee.setPersonalPhoneNumber(personalPhoneNumberTextArea.getText());
        employee.setBusinessMobilePhoneNumber(businessMobilePhoneNumberTextArea.getText());

        searchByEmployeeAndSubdivision(employee);
    }

    @FXML
    /**
     * Действия при нажатии кнопки добавить структурное подразделние.
     */
    void addSubdivisionButtonAction(ActionEvent event) {
        if (subdivisionTreeView.getSelectionModel().getSelectedItem() == null && subdivisionTreeView.getRoot() != null) {
            showErrorDialog("Выберите подразделение");
            subdivisionTreeView.requestFocus();
        } else if (subdivisionTextEdit.getText() == null || subdivisionTextEdit.getText().isEmpty()) {
            showErrorDialog("Введите название подразделения");
            subdivisionTextEdit.requestFocus();
        } else {

            Subdivision subdivision = new Subdivision(-1, subdivisionTextEdit.getText());
            if (subdivisionTreeView.getRoot() != null)
                subdivision.setParent_id(subdivisionTreeView.getSelectionModel().getSelectedItem().getValue().getId());
            subdivisionDAO.addNewSubdivision(subdivision);

            if (subdivision.getId() > 0) {
                TreeItem<Subdivision> treeItem = new TreeItem<>(subdivision);
                if (subdivisionTreeView.getRoot() != null) {
                    subdivisionTreeView.getSelectionModel().getSelectedItem().getChildren().add(treeItem);
                    subdivisionTreeView.getSelectionModel().getSelectedItem().setExpanded(true);
                } else {//при пустом дереве структурных подразделений, создание корня
                    subdivisionTreeView.setRoot(treeItem);
                }

            }
        }
    }

    @FXML
    /**
     * Действия при нажатии кнопки фильтр по подразделению выбранному в TreeView.
     */
    void filterBySubdivisionButtonAction(ActionEvent event) {
        searchByEmployeeAndSubdivision(new Employee());
    }

    @FXML
    /**
     * Действия при нажатии кнопки удалить подразделение.
     * Удаляет также всех сотрудников текущего подразделения и сотрудников во всех потомках подразделения.
     */
    void removeSubdivisionButtonAction(ActionEvent event) {
        if (subdivisionTreeView.getSelectionModel().getSelectedItem() == null) {
            showErrorDialog("Выберите подразделение");
            subdivisionTreeView.requestFocus();
        } else if (showConfirmationDialog("Вы действительно хоти удалить выделенные записи?\n" +
                "Удаляться и все сотрудники входящие в подразделения и вниз по иерархиию")) {
            ArrayList<Integer> subdivisionIdList = new ArrayList<>();

            subdivisionIdList.add(subdivisionTreeView.getSelectionModel().getSelectedItem().getValue().getId());
            getAllChildrenIdFromSubdivision(subdivisionTreeView.getSelectionModel().getSelectedItem(), subdivisionIdList);
            for (Integer id : subdivisionIdList
                    ) {
                subdivisionDAO.removeSubdivision(id);
                employeeDAO.removeEmployeeBySubdivisionId(id);
            }

            TreeItem<Subdivision> subdivisionTreeItem = subdivisionTreeView.getSelectionModel().getSelectedItem();
            subdivisionTreeItem.getParent().getChildren().remove(subdivisionTreeItem);

            subdivisionTreeView.getSelectionModel().select(0);//выделение корня
            searchByEmployeeAndSubdivision(new Employee());
        }
    }

    @FXML
    /**
     * Действия при нажатии кнопки сброс фильтра.
     */
    void resetFilterBySubdivisionButtonAction(ActionEvent event) {
        try {
            List<Employee> employeeList = employeeDAO.searchEmployee(new Employee(), new ArrayList<>());

            if (employeeList.isEmpty()) {
                showErrorDialog("Ошибка при загрузке данных из БД");
            } else {

                tableViewEmployees.setItems(FXCollections.observableArrayList(employeeList));
                subdivisionTreeView.getSelectionModel().select(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    /**
     * Действия при нажатии кнопки редактирование имени подразделния.
     */
    void editSubdivisionButtonAction(ActionEvent event) {
        if (subdivisionTreeView.getSelectionModel().getSelectedItem() == null) {
            showErrorDialog("Выберите подразделение");
            subdivisionTreeView.requestFocus();
        } else if (subdivisionTextEdit.getText() == null || subdivisionTextEdit.getText().isEmpty()) {
            showErrorDialog("Введите название подразделения");
            subdivisionTextEdit.requestFocus();
        } else {
            Subdivision subdivision = subdivisionTreeView.getSelectionModel().getSelectedItem().getValue();
            subdivision.setSubdivisionName(subdivisionTextEdit.getText());
            subdivisionDAO.updateSubdivision(subdivision);
            subdivisionTreeView.refresh();
            tableViewEmployees.refresh();
        }
    }

    @Override
    /**
     * Инициализация и настройка формы.
     */
    public void initialize(URL url, ResourceBundle rb) {

        employeeDAO = new EmployeeDAO();
        subdivisionDAO = new SubdivisionDAO();

        //сопоставление полей таблицы
        idEmployees.setCellValueFactory(new PropertyValueFactory<>("id"));
        fullNameEmployees.setCellValueFactory(new PropertyValueFactory<>("fullName"));
        positionEmployees.setCellValueFactory(new PropertyValueFactory<>("position"));
        subdivisionEmployees.setCellValueFactory(new PropertyValueFactory<>("subdivisionName"));
        businessPhoneNumberEmployees.setCellValueFactory(new PropertyValueFactory<>("businessPhoneNumber"));
        personalPhoneNumberEmployees.setCellValueFactory(new PropertyValueFactory<>("personalPhoneNumber"));
        businessMobilePhoneNumberEmployees.setCellValueFactory(new PropertyValueFactory<>("businessMobilePhoneNumber"));

        resetFilterBySubdivisionButtonAction(new ActionEvent());
        //возможность выбирать несколько строк в таблице сотрудников
        tableViewEmployees.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        //создание дерева подразделений
        TreeNode<Subdivision> subdivisionTreeNode = subdivisionDAO.getAllSubdivision();
        TreeItem<Subdivision> rootItem = new TreeItem<>(subdivisionTreeNode.getData());
        rootItem.setExpanded(true);
        buildSubdivisionTreeFromRoot(rootItem, subdivisionTreeNode);
        subdivisionTreeView.setRoot(rootItem);

        //заполение полей при выделении записи в таблице сотрудников
        tableViewEmployees.getSelectionModel()
                .selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        fullNameTextEdit.setText(newValue.getFullName());
                        positionTextEdit.setText(newValue.getPosition());
                        businessPhoneNumberTextArea.setText(newValue.getBusinessPhoneNumber());
                        personalPhoneNumberTextArea.setText(newValue.getPersonalPhoneNumber());
                        businessMobilePhoneNumberTextArea.setText(newValue.getBusinessMobilePhoneNumber());
                        TreeItem<Subdivision> subdivisionTreeItem = new TreeItem<>(new Subdivision(newValue.getSubdivisionId(),
                                newValue.getSubdivisionName()));
                        subdivisionTreeView.getSelectionModel().select(subdivisionTreeItem);
                    }
                });

        //заполение полей при выделении записи в таблице сотрудников
        subdivisionTreeView.getSelectionModel()
                .selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue != null)
                        subdivisionTextEdit.setText(newValue.getValue().getSubdivisionName());
                });
    }

    /**
     * Окно информации.
     *
     * @param information .
     */
    private void showInformationDialog(String information) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Информация");
        alert.setHeaderText(null);
        alert.setContentText(information);

        alert.showAndWait();
    }

    /**
     * Окно ошибки.
     *
     * @param error .
     */
    private void showErrorDialog(String error) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Ошибка");
        alert.setHeaderText(null);
        alert.setContentText(error);

        alert.showAndWait();
    }

    /**
     * Окно подтверждения.
     *
     * @param confirmtion .
     * @return
     */
    private boolean showConfirmationDialog(String confirmtion) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Подтверждение");
        alert.setHeaderText(null);
        alert.setContentText(confirmtion);

        Optional<ButtonType> buttonTypeOptional = alert.showAndWait();

        if (buttonTypeOptional.get() == ButtonType.OK)
            return true;
        else
            return false;
    }
}
