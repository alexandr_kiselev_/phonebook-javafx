package model;

import beans.Subdivision;
import beans.TreeNode;
import util.ConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Класс работы с БД для объекта структурное подразделение.
 */
public class SubdivisionDAO {

    private Connection connection;

    public SubdivisionDAO() {
        this.connection = new ConnectionFactory().getConnection();
    }

    /**
     * Добавление нового структурного подразделения в БД.
     * @param subdivision .
     */
    public void addNewSubdivision(Subdivision subdivision) {

        String sql = "INSERT INTO subdivision (name, parent_id) VALUES (?, ?) RETURNING id";
        if (subdivision.getParent_id() == null)
            sql = "INSERT INTO subdivision (name) VALUES (?) RETURNING id";

        try {

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, subdivision.getSubdivisionName());
            if (subdivision.getParent_id() != null)
                preparedStatement.setInt(2, subdivision.getParent_id());

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) ;
            {
                subdivision.setId(resultSet.getInt("id"));
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Редкатирование подразделения в БД
     * @param subdivision
     */
    public void updateSubdivision(Subdivision subdivision) {

        String sql = "UPDATE subdivision SET name=? WHERE id=?";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, subdivision.getSubdivisionName());
            preparedStatement.setInt(2, subdivision.getId());
            preparedStatement.execute();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Удаление подразделения в БД.
     * @param subdivision_id .
     */
    public void removeSubdivision(int subdivision_id) {
        String sql = "DELETE FROM subdivision WHERE id=?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, subdivision_id);
            preparedStatement.execute();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Создание структуры дерева подразделений по данным из БД.
     * @param subdivisionTreeNode .
     * @param subdivision .
     */
    private void setChildren(TreeNode<Subdivision> subdivisionTreeNode, Subdivision subdivision) {
        if (subdivisionTreeNode.getData().getId() == subdivision.getParent_id()) {
            subdivisionTreeNode.addChild(subdivision);
        } else if (!subdivisionTreeNode.getChildren().isEmpty()) {
            for (TreeNode<Subdivision> treeItem : subdivisionTreeNode.getChildren()
                    ) {
                if (treeItem.getData().getId() == subdivision.getParent_id()) {
                    treeItem.addChild(subdivision);
                    return;
                } else {
                    setChildren(treeItem, subdivision);
                }
            }
        }
    }

    /**
     * Врзвращает дерево подразделений по данным из БД.
     * @return
     */
    public TreeNode<Subdivision> getAllSubdivision() {

        String sql = "SELECT * FROM subdivision  ORDER BY parent_id=null, id, name";
        Subdivision subdivision = null;
        TreeNode<Subdivision> subdivisionTreeNode = null;

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                subdivision = new Subdivision(resultSet.getInt("id"), resultSet.getString("name"));
                subdivisionTreeNode = new TreeNode<>(subdivision);
            }

            while (resultSet.next()) {
                subdivision = new Subdivision(resultSet.getInt("id"), resultSet.getString("name"));
                subdivision.setParent_id(resultSet.getInt("parent_id"));
                setChildren(subdivisionTreeNode, subdivision);
            }

            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return subdivisionTreeNode;
    }
}
