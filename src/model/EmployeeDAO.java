package model;

import beans.Employee;
import util.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс работы с БД для объекта сотрудник.
 */
public class EmployeeDAO {

    private Connection connection;

    public EmployeeDAO() {
        this.connection = new ConnectionFactory().getConnection();
    }

    /**
     * Добавление нового сотрудника в БД.
     * @param employee .
     */
    public void addNewEmployee(Employee employee) {

        String sql = "INSERT INTO employee (fullname, position, subdivision_id,  business_phone_number, "
                + "personal_phone_number, business_mobile_phone_number) VALUES (?, ?, ?, ?, ?, ?) RETURNING id";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, employee.getFullName());
            preparedStatement.setString(2, employee.getPosition());
            preparedStatement.setInt(3, employee.getSubdivisionId());
            preparedStatement.setString(4, employee.getBusinessPhoneNumber());
            preparedStatement.setString(5, employee.getPersonalPhoneNumber());
            preparedStatement.setString(6, employee.getBusinessMobilePhoneNumber());
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) ;
            {
                employee.setId(resultSet.getInt("id"));
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * Редактирование сотрудника в БД.
     * @param employee .
     */
    public void updateEmployee(Employee employee) {

        String sql = "UPDATE employee SET fullname=?, position=?, subdivision_id=?,  business_phone_number=?, " +
                "personal_phone_number=?, business_mobile_phone_number=? WHERE id=?";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, employee.getFullName());
            preparedStatement.setString(2, employee.getPosition());
            preparedStatement.setInt(3, employee.getSubdivisionId());
            preparedStatement.setString(4, employee.getBusinessPhoneNumber());
            preparedStatement.setString(5, employee.getPersonalPhoneNumber());
            preparedStatement.setString(6, employee.getBusinessMobilePhoneNumber());
            preparedStatement.setInt(7, employee.getId());
            preparedStatement.execute();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * Удаление сотрудника в БД.
     * @param employee_id .
     */
    public void removeEmployee(int employee_id) {
        String sql = "DELETE FROM employee WHERE id=?";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, employee_id);
            preparedStatement.execute();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * Удаление сотрудникиков в БД по id структурного подразделения.
     * @param subdivisionId .
     */
    public void removeEmployeeBySubdivisionId(int subdivisionId) {
        String sql = "DELETE FROM employee WHERE subdivision_id=?";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, subdivisionId);
            preparedStatement.execute();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Поиск сотрудников по заданным критериям.
     * @param searchEmployee .
     * @param subdivisions .
     * @return
     */
    public List<Employee> searchEmployee(Employee searchEmployee, List<Integer> subdivisions) {

        List<Employee> employeeList = new ArrayList<>();

        if (searchEmployee.getFullName() == null)
            searchEmployee.setFullName("");
        if (searchEmployee.getPosition() == null)
            searchEmployee.setPosition("");
        if (searchEmployee.getBusinessPhoneNumber() == null)
            searchEmployee.setBusinessPhoneNumber("");
        if (searchEmployee.getPersonalPhoneNumber() == null)
            searchEmployee.setPersonalPhoneNumber("");
        if (searchEmployee.getBusinessMobilePhoneNumber() == null)
            searchEmployee.setBusinessMobilePhoneNumber("");

        StringBuilder sqlSubdivision = new StringBuilder();
        if (!subdivisions.isEmpty()) {
            sqlSubdivision.append(" AND subdivision_id IN(");
            for (int subdivision : subdivisions
                    ) {
                sqlSubdivision.append(subdivision + ",");
            }
            sqlSubdivision.setLength(sqlSubdivision.length() - 1);
            sqlSubdivision.append(")");
        }

        String sql = "SELECT a.*, b.name AS subdivisionname, b.id AS subdivisionId " +
                "FROM employee AS a JOIN subdivision AS b ON a.subdivision_id = b.id WHERE fullname LIKE '%" +
                searchEmployee.getFullName() + "%' AND \"position\" LIKE '%" + searchEmployee.getPosition() +
                "%' AND business_phone_number LIKE '%" + searchEmployee.getBusinessPhoneNumber() +
                "%' AND personal_phone_number LIKE '%" + searchEmployee.getPersonalPhoneNumber() +
                "%' AND business_mobile_phone_number LIKE '%" + searchEmployee.getBusinessMobilePhoneNumber() +
                "%' " + sqlSubdivision.toString() + " ORDER BY fullname";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Employee employee = new Employee();
                employee.setId(resultSet.getInt("id"));
                employee.setFullName(resultSet.getString("fullname"));
                employee.setPosition(resultSet.getString("position"));
                employee.setSubdivisionId(resultSet.getInt("subdivisionId"));
                employee.setSubdivisionName(resultSet.getString("subdivisionname"));
                employee.setBusinessPhoneNumber(resultSet.getString("business_phone_number"));
                employee.setPersonalPhoneNumber(resultSet.getString("personal_phone_number"));
                employee.setBusinessMobilePhoneNumber(resultSet.getString("business_mobile_phone_number"));
                employeeList.add(employee);
            }

            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return employeeList;
    }
}
