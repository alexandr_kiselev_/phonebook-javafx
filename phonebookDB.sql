SET client_encoding = 'UTF8';
SET search_path = public, pg_catalog;

CREATE SEQUENCE public."Employee_id_seq"
    INCREMENT 1
    START 24
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE SEQUENCE public.subdivision_id_seq
    INCREMENT 1
    START 40
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE 1;    

CREATE TABLE employee (
    id integer DEFAULT nextval('"Employee_id_seq"'::regclass) NOT NULL,
    fullname character varying(80),
    "position" character varying(50),
    subdivision_id integer,
    business_phone_number character varying(60),
    personal_phone_number character varying(60),
    business_mobile_phone_number character varying(60)
);

INSERT INTO employee (fullname, "position", id, subdivision_id, business_phone_number, personal_phone_number, business_mobile_phone_number) VALUES ('Пупкин Петр Петрович', 'Начальник отдела', 6, 3, '+375291111111
+375291111112', '+375291111115', '+375291111119');
INSERT INTO employee (fullname, "position", id, subdivision_id, business_phone_number, personal_phone_number, business_mobile_phone_number) VALUES ('Иванов Иван Иванович', 'Директор', 19, 0, '+375290000001
+375290000002', '+375290000011
+375290000022', '+375290000111
+375290000222');
INSERT INTO employee (fullname, "position", id, subdivision_id, business_phone_number, personal_phone_number, business_mobile_phone_number) VALUES ('Васильев Василий Васильевич', 'Начальник отдела', 3, 4, '+375290000034', '+375290000344', '+375290003444');
INSERT INTO employee (fullname, "position", id, subdivision_id, business_phone_number, personal_phone_number, business_mobile_phone_number) VALUES ('Сидоров Антон Антонович', 'Техник-программист', 4, 5, '+375290000443', '+375290004443', '+375290044443');
INSERT INTO employee (fullname, "position", id, subdivision_id, business_phone_number, personal_phone_number, business_mobile_phone_number) VALUES ('Ромашкин Глеб Вахтангович', 'Инженер-программист', 2, 1, '+375290000002', '+375290000022', '+375290000222');

ALTER TABLE ONLY employee
    ADD CONSTRAINT employee_pkey PRIMARY KEY (id);

CREATE TABLE subdivision (
    id integer DEFAULT nextval('subdivision_id_seq'::regclass) NOT NULL,
    name character varying(60),
    parent_id integer
);

INSERT INTO subdivision (id, name, parent_id) VALUES (4, 'Сектор1', 1);
INSERT INTO subdivision (id, name, parent_id) VALUES (5, 'Сектор2', 1);
INSERT INTO subdivision (id, name, parent_id) VALUES (1, 'ОИТ', 0);
INSERT INTO subdivision (id, name, parent_id) VALUES (7, 'Подсектор 1', 5);
INSERT INTO subdivision (id, name, parent_id) VALUES (40, 'Подсектор 0', 4);
INSERT INTO subdivision (id, name, parent_id) VALUES (2, 'Бухгалетрия', 0);
INSERT INTO subdivision (id, name, parent_id) VALUES (3, 'Маркетинг', 0);
INSERT INTO subdivision (id, name, parent_id) VALUES (8, 'Диспетчерская', 0);
INSERT INTO subdivision (id, name, parent_id) VALUES (0, 'Компания', NULL);

ALTER TABLE ONLY subdivision
    ADD CONSTRAINT subdivision_pkey PRIMARY KEY (id);

